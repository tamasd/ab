// Copyright 2015 Tamás Demeter-Haludka
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ab

import (
	"compress/gzip"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"database/sql"
	"encoding/hex"
	"errors"
	"io/ioutil"
	stdlog "log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"golang.org/x/crypto/acme"
	"golang.org/x/crypto/acme/autocert"

	"github.com/NYTimes/gziphandler"
	"github.com/julienschmidt/httprouter"
	"github.com/spf13/viper"
	"gitlab.com/tamasd/ab/lib/log"
	"gitlab.com/tamasd/ab/util"
)

const paramKey = "abparam"

// Service is a collection of endpoints that logically belong together or operate on the same part of the database schema.
type Service interface {
	// Register the Service endpoints
	Register(*Server) error
	// Checks if the schema is installed
	SchemaInstalled(db DB) bool
	// Construct SQL string to install the schema
	SchemaSQL() string
}

// Hop sets up and starts a server.
//
// This function is a wrapper around PetBunny(). The cfg parameter can be nil, in that case a new one will be created.
//
// Extra viper values:
//
// - secret: sets util.SetKey(). Must be a 32 byte hex-encoded string.
func Hop(configure func(cfg *viper.Viper, s *Server) error, cfg *viper.Viper, topMiddlewares ...func(http.Handler) http.Handler) {
	logger := log.DefaultOSLogger()
	if cfg == nil {
		cfg = viper.New()
	}
	cfg.SetConfigName("config")
	cfg.AddConfigPath(".")
	cfg.AutomaticEnv()
	cfg.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if err := cfg.ReadInConfig(); err != nil {
		logger.Verbose().Println(err)
	}

	secret, err := hex.DecodeString(cfg.GetString("secret"))
	if err != nil {
		logger.Fatalln(err)
	}
	if err := util.SetKey(secret); err != nil {
		logger.Fatalln(err)
	}

	s, err := PetBunny(cfg, logger, topMiddlewares...)

	if err != nil {
		logger.Fatalln(err)
	}

	if err := configure(cfg, s); err != nil {
		logger.Fatalln(err)
	}

	cfg.SetDefault("host", "localhost")
	cfg.SetDefault("port", "8080")

	addr := cfg.GetString("host") + ":" + cfg.GetString("port")
	certFile := cfg.GetString("certfile")
	keyFile := cfg.GetString("keyfile")

	if err := s.StartHTTPS(addr, certFile, keyFile); err != nil {
		logger.Fatalln(err)
	}
}

// PetBunny sets up a Server with recommended middlewares.
//
// The parameters logger and eh can be nil, defaults will be log.DefaultOSLogger() and HandleErrors().
//
// topMiddlewares are middlewares that gets applied right after the logger middlewares, but before anything else.
//
// Viper has to be set up, and it has to contain a few values:
//
// - CookieSecret string: hex representation of the key bytes. Must be set.
//
// - db string: connection string to Postgres. Must be set.
//
// - DBMaxIdleConn int: max idle connections. Defaults to 0 (no open connections are retained).
//
// - DBMaxOpenConn int: max open connections. Defaults to 0 (unlimited).
//
// - LogLevel int: log level for the logger. Use the numeric values of the log.LOG_* constants.
//
// - hsts (hsts.maxage float, hsts.includesubdomains bool, hsts.hostblacklist []string): configuration values for the HSTS middleware. See HSTSConfig structure
//
// - gzip bool: enabled the gzip middleware. Default is true.
//
// - CookiePrefix string: prefix for the session and the csrf cookies.
//
// - CookieURL string: domain and path configuration for the cookies.
//
// - assetsDir string: assets directory. The value - skips setting it up.
//
// - publicDir string: public directory. The value - skips setting it up.
//
// - root bool: sets / to serve assetsDir/index.html. Default is true.
//
// - hostwhitelist: a string array of hosts that are whitelisted for letsencrypt. If set, https will be enabled with letsencrypt.
func PetBunny(cfg *viper.Viper, logger *log.Log, topMiddlewares ...func(http.Handler) http.Handler) (*Server, error) {
	cookieSecret := cfg.GetString("CookieSecret")
	if cookieSecret == "" {
		return nil, errors.New("secret key must not be empty")
	}
	cookieSecretBytes, err := hex.DecodeString(cookieSecret)
	if err != nil {
		return nil, err
	}

	m, conn := DBMiddleware(cfg.GetString("db"), cfg.GetInt("DBMaxIdleConn"), cfg.GetInt("DBMaxOpenConn"))

	s := NewServer(conn)

	if logger != nil {
		s.Logger = logger
	}

	s.Logger.Level = log.LogLevel(cfg.GetInt("LogLevel"))

	if len(topMiddlewares) > 0 {
		s.Use(topMiddlewares...)
	}

	s.Use(RequestIDMiddleware)

	requestLoggerOut := ioutil.Discard
	if s.Logger.Level > log.LOG_USER {
		requestLoggerOut = os.Stdout
	}

	s.Use(RequestLoggerMiddleware(requestLoggerOut))

	s.Use(DefaultLoggerMiddleware(s.Logger.Level))

	if cfg.IsSet("hsts") {
		hsts := &HSTSConfig{}
		cfg.UnmarshalKey("hsts", hsts)
		s.Use(HSTSMiddleware(*hsts))
	}

	cfg.SetDefault("gzip", true)
	if cfg.GetBool("gzip") {
		s.Use(gziphandler.MustNewGzipLevelHandler(gzip.BestCompression))
	}

	displayErrors := s.Logger.Level > log.LOG_USER
	if cfg.IsSet("displayerrors") {
		displayErrors = cfg.GetBool("displayerrors")
	}
	s.Use(ErrorHandlerMiddleware(displayErrors))

	s.Use(RendererMiddleware)

	cookiePrefix := cfg.GetString("CookiePrefix")
	var cookieURL *url.URL = nil
	if cfg.IsSet("CookieURL") {
		cookieURL, err = url.Parse(cfg.GetString("CookieURL"))
	}

	s.Use(SessionMiddleware(cookiePrefix, SecretKey(cookieSecretBytes), cookieURL, time.Hour*24*365))

	s.Use(CSRFMiddleware)
	s.Get("/api/token", http.HandlerFunc(CSRFTokenHandler))

	s.Use(m)

	cfg.SetDefault("assetsDir", "assets")
	cfg.SetDefault("publicDir", "public")

	assetsDir := cfg.GetString("assetsDir")
	publicDir := cfg.GetString("publicDir")

	if assetsDir != "-" {
		s.AddLocalDir("/assets", assetsDir)
	}
	if publicDir != "-" {
		s.AddLocalDir("/public", publicDir)
	}

	cfg.SetDefault("root", true)
	if cfg.GetBool("root") {
		s.AddFile("/", assetsDir+"/index.html")
	}

	hostWhitelist := cfg.GetStringSlice("hostwhitelist")
	if len(hostWhitelist) > 0 {
		s.EnableLetsEncrypt("", hostWhitelist...)
	}

	return s, nil
}

// Server is the main server struct.
type Server struct {
	*httprouter.Router
	conn        *sql.DB
	middlewares []func(http.Handler) http.Handler
	Logger      *log.Log
	TLSConfig   *tls.Config
}

// NewServer creates a new server with a database connection.
func NewServer(conn *sql.DB) *Server {
	router := httprouter.New()
	router.HandleMethodNotAllowed = false
	s := &Server{
		Router: router,
		conn:   conn,
		Logger: log.DefaultOSLogger(),
	}

	return s
}

// Use adds middlewares to the end of the middleware chain.
func (s *Server) Use(middleware ...func(http.Handler) http.Handler) {
	s.middlewares = append(s.middlewares, middleware...)
}

// UseTop adds middlewares to the beginning of the middleware chain.
func (s *Server) UseTop(middlewares ...func(http.Handler) http.Handler) {
	s.middlewares = append(middlewares, s.middlewares...)
}

// UseHandler uses a http.Handler as a middleware.
func (s *Server) UseHandler(h http.Handler) {
	s.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			h.ServeHTTP(w, r)
			next.ServeHTTP(w, r)
		})
	})
}

// Handler creates a http.Handler from the server (using the middlewares and the router).
func (s *Server) Handler() http.Handler {
	return wrapHandler(s.Router, s.middlewares...)
}

func wrapHandler(handler http.Handler, middlewares ...func(http.Handler) http.Handler) http.Handler {
	for i := len(middlewares) - 1; i >= 0; i-- {
		handler = middlewares[i](handler)
	}

	return handler
}

// Handle adds a handler to the router.
//
// The middleware list will be applied to this handler only.
func (s *Server) Handle(method, path string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) {
	handler = wrapHandler(handler, middlewares...)
	s.Router.Handle(method, path, httprouter.Handle(func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		r = SetContext(r, paramKey, p)
		handler.ServeHTTP(w, r)
	}))
}

// Head adds a HEAD handler to the router.
func (s *Server) Head(path string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) {
	s.Handle("HEAD", path, handler, middlewares...)
}

// Get adds a GET handler to the router.
func (s *Server) Get(path string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) {
	s.Handle("GET", path, handler, middlewares...)
}

// Post adds a POST handler to the router.
func (s *Server) Post(path string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) {
	s.Handle("POST", path, handler, middlewares...)
}

// Put adds a PUT handler to the router.
func (s *Server) Put(path string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) {
	s.Handle("PUT", path, handler, middlewares...)
}

// Delete adds a DELETE handler to the router.
func (s *Server) Delete(path string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) {
	s.Handle("DELETE", path, handler, middlewares...)
}

// Patch adds a PATCH handler to the router.
func (s *Server) Patch(path string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) {
	s.Handle("PATCH", path, handler, middlewares...)
}

// Options adds an OPTIONS handler to the router.
func (s *Server) Options(path string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) {
	s.Handle("OPTIONS", path, handler, middlewares...)
}

// HeadF adds a HEAD HandlerFunc to the router.
func (s *Server) HeadF(path string, handler http.HandlerFunc, middlewares ...func(http.Handler) http.Handler) {
	s.Handle("HEAD", path, handler, middlewares...)
}

// GetF adds a GET HandlerFunc to the router.
func (s *Server) GetF(path string, handler http.HandlerFunc, middlewares ...func(http.Handler) http.Handler) {
	s.Handle("GET", path, handler, middlewares...)
}

// PostF adds a POST HandlerFunc to the router.
func (s *Server) PostF(path string, handler http.HandlerFunc, middlewares ...func(http.Handler) http.Handler) {
	s.Handle("POST", path, handler, middlewares...)
}

// PutF adds a PUT HandlerFunc to the router.
func (s *Server) PutF(path string, handler http.HandlerFunc, middlewares ...func(http.Handler) http.Handler) {
	s.Handle("PUT", path, handler, middlewares...)
}

// DeleteF adds a DELETE HandlerFunc to the router.
func (s *Server) DeleteF(path string, handler http.HandlerFunc, middlewares ...func(http.Handler) http.Handler) {
	s.Handle("DELETE", path, handler, middlewares...)
}

// PatchF adds a PATCH HandlerFunc to the router.
func (s *Server) PatchF(path string, handler http.HandlerFunc, middlewares ...func(http.Handler) http.Handler) {
	s.Handle("PATCH", path, handler, middlewares...)
}

// OptionsF adds an OPTIONS HandlerFunc to the router.
func (s *Server) OptionsF(path string, handler http.HandlerFunc, middlewares ...func(http.Handler) http.Handler) {
	s.Handle("OPTIONS", path, handler, middlewares...)
}

// GetParams returns the path parameter values from the request.
func GetParams(r *http.Request) httprouter.Params {
	return r.Context().Value(paramKey).(httprouter.Params)
}

// GetDBConnection returns the server's DB connection if there's any.
func (s *Server) GetDBConnection() DB {
	return s.conn
}

// AddLocalDir adds a local directory to the router.
func (s *Server) AddLocalDir(prefix, path string) *Server {
	s.ServeFiles(prefix+"/*filepath", http.Dir(path))

	return s
}

// Adds a local file to the router.
func (s *Server) AddFile(path, file string) *Server {
	s.Get(path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, file)
	}))

	return s
}

// RegisterService adds a service on the server.
//
// See the Service interface for more information.
func (s *Server) RegisterService(svc Service) {
	svc.Register(s)
	if s.conn != nil && !svc.SchemaInstalled(s.conn) {
		sql := svc.SchemaSQL()
		_, err := s.conn.Exec(sql)
		if err != nil {
			panic(err.Error() + "\n" + sql)
		}
	}
}

// StartHTTPS starts the server.
func (s *Server) StartHTTPS(addr, certFile, keyFile string) error {
	return s.startServer(addr, certFile, keyFile, false)
}

func (s *Server) startServer(addr, certFile, keyFile string, forceHTTP bool) error {
	srv := &http.Server{
		Addr:      addr,
		Handler:   s.Handler(),
		TLSConfig: s.TLSConfig,
	}

	s.Logger.User().Printf("Starting server on %s\n", addr)

	if stdlogger, ok := s.Logger.User().(*stdlog.Logger); ok {
		srv.ErrorLog = stdlogger
	}

	var err error
	if !forceHTTP && ((certFile != "" && keyFile != "") || srv.TLSConfig != nil) {
		err = srv.ListenAndServeTLS(certFile, keyFile)
	} else {
		err = srv.ListenAndServe()
	}

	return err
}

// StartHTTP starts the server.
func (s *Server) StartHTTP(addr string) error {
	return s.startServer(addr, "", "", true)
}

// EnableAutocert adds autocert to the server.
//
// caDirEndpoint points to a ca directory endpoint. cacheDir defaults to "private/autocert-cache"
// when empty. If you use the recommended app layout, leave it empty. hostWhitelist is a list of
// hosts where the SSL certificate is valid. Supply at least one domain, else it won't work.
func (s *Server) EnableAutocert(caDirEndpoint, cacheDir string, hostWhitelist ...string) {
	key, _ := rsa.GenerateKey(rand.Reader, 2048)
	client := &acme.Client{
		Key:          key,
		DirectoryURL: caDirEndpoint,
	}

	if cacheDir == "" {
		cacheDir = "private/autocert-cache"
	}

	m := autocert.Manager{
		Client:     client,
		Prompt:     autocert.AcceptTOS,
		HostPolicy: autocert.HostWhitelist(hostWhitelist...),
		Cache:      autocert.DirCache(cacheDir),
	}

	if s.TLSConfig == nil {
		s.TLSConfig = &tls.Config{}
	}

	s.TLSConfig.GetCertificate = m.GetCertificate
}

// EnableLetsEncrypt adds autocert to the server with LetsEncrypt CA dir.
//
// See the documentation of EnableAutocert for cacheDir and hostWhitelist
func (s *Server) EnableLetsEncrypt(cacheDir string, hostWhitelist ...string) {
	s.EnableAutocert(acme.LetsEncryptURL, cacheDir, hostWhitelist...)
}

// HTTPSRedirectServe redirects HTTP requests to HTTPS.
//
// httpsAddr and httpAddr must be host:port format, where the port can be omitted.
func HTTPSRedirectServer(httpsAddr, httpAddr string) error {
	srv := &http.Server{
		Addr: httpAddr,
	}

	srv.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		newUrl := "https://" + httpsAddr + "/" + r.RequestURI
		LogTrace(r).Printf("Redirecting %s to %s via HTTPSRedirectServer\n", r.URL.String(), newUrl)
		http.Redirect(w, r, newUrl, http.StatusMovedPermanently)
	})

	return srv.ListenAndServe()
}

// RedirectServer sets up and starts a http server that redirects all requests to redirectAddr.
func RedirectServer(addr, redirectAddr, certFile, keyFile string) error {
	srv := &http.Server{
		Addr: addr,
	}

	srv.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		proto := "http"
		if r.TLS != nil {
			proto = "https"
		}
		newUrl := proto + "://" + redirectAddr + "/" + r.RequestURI

		LogTrace(r).Printf("Redirecting %s to %s via RedirectServer\n", r.URL.String(), newUrl)

		http.Redirect(w, r, newUrl, http.StatusMovedPermanently)
	})

	if certFile != "" && keyFile != "" {
		return srv.ListenAndServeTLS(certFile, keyFile)
	} else {
		return srv.ListenAndServe()
	}
}
