// Copyright 2015 Tamás Demeter-Haludka
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ab

import (
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/agtorre/gocolorize"
)

// RequestLoggerMiddleware logs request data (method, length, path).
func RequestLoggerMiddleware(lw io.Writer) func(http.Handler) http.Handler {
	reqidColor := gocolorize.NewColor("red")
	http1xxColor := gocolorize.NewColor("black+b:white")
	http2xxColor := gocolorize.NewColor("white+b:green")
	http3xxColor := gocolorize.NewColor("white+b:blue")
	http4xxColor := gocolorize.NewColor("white+b:yellow")
	http5xxColor := gocolorize.NewColor("white+b:red")
	methodColor := gocolorize.NewColor("cyan")
	pathColor := gocolorize.NewColor("blue")
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			var start, end int64
			starttime := time.Now()
			start = starttime.UnixNano()

			path := r.URL.Path
			host := r.Host
			protocol := "http"
			if r.TLS != nil {
				protocol = "https"
			}

			prefix := ""

			if reqid := GetRequestID(r); reqid != "" {
				prefix += reqidColor.Paint(reqid) + " "
			}

			rw := &requestLoggerResponseWriter{
				ResponseWriter: w,
				code:           http.StatusOK,
			}

			next.ServeHTTP(rw, r)

			end = time.Now().UnixNano()
			duration := end - start
			time := ""
			if duration >= 1000000000 {
				time = fmt.Sprintf("%.2fs", float64(duration)/1000000000)
			} else if duration >= 1000000 {
				time = fmt.Sprintf("%.2fms", float64(duration)/1000000)
			} else if duration >= 1000 {
				time = fmt.Sprintf("%.2fµs", float64(duration)/1000)
			} else {
				time = fmt.Sprintf("%ldns", duration)
			}

			httpCode := rw.GetCode()
			code := fmt.Sprintf("%d", httpCode)
			if httpCode >= 100 && httpCode <= 199 {
				code = http1xxColor.Paint(code)
			} else if httpCode >= 200 && httpCode <= 299 {
				code = http2xxColor.Paint(code)
			} else if httpCode >= 300 && httpCode <= 399 {
				code = http3xxColor.Paint(code)
			} else if httpCode >= 400 && httpCode <= 499 {
				code = http4xxColor.Paint(code)
			} else if httpCode >= 500 && httpCode <= 599 {
				code = http5xxColor.Paint(code)
			}

			fmt.Fprintf(lw, "%s%s\t%s\t%s\t%s\t%s\n", prefix, methodColor.Paint(r.Method), pathColor.Paint(protocol+"://"+host+path), code, starttime.Format("2006/01/02 15:04:05"), time)
		})
	}
}

var _ http.ResponseWriter = &requestLoggerResponseWriter{}

type requestLoggerResponseWriter struct {
	http.ResponseWriter
	code int
}

func (rw *requestLoggerResponseWriter) WriteHeader(code int) {
	rw.code = code
	rw.ResponseWriter.WriteHeader(code)
}

func (rw *requestLoggerResponseWriter) GetCode() int {
	return rw.code
}
