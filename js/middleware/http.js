// Copyright 2016 Tamás Demeter-Haludka
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import {request} from "../http";

function isCached(state, action) {
	if (action.http.skipCache) {
		return false;
	}

	const storeType = action.http.storeType || "listitem";
	switch (storeType) {
		case "list":
			return !!(state[action.http.store]);
			break;
		case "listitem":
			if (action.http.cacheKey) {
				return !!(state[action.http.store] ? state[action.http.store][action.http.cacheKey] : false);
			}
			break;
		case "singlekey":
			return !((state[action.http.store] || null) === null);
			break;
	}

	return false;
}

export default store => next => action => {
	if (action.http && !action.httpLoading && !action.httpSuccess && !action.httpError && !isCached(store.getState(), action)) {
		action = Object.assign({}, action, {
			httpLoading: true,
		});
		const headers = action.http.headers || new Headers();
		if (!headers.has("X-CSRF-Token")) {
			headers.set("X-CSRF-Token", store.getState().csrfToken);
		}
		request(
			action.http.method || "GET",
			action.http.url,
			action.http.data ? JSON.stringify(action.http.data) : null,
			headers
		).then(function(response) {
			store.dispatch(Object.assign({}, action, {
				httpLoading: false,
				httpSuccess: response,
			}));
		}, function(reason) {
			store.dispatch(Object.assign({}, action, {
				httpLoading: false,
				httpError: reason,
			}));
		});
	}
	let result = next(action);
	return result;
};
