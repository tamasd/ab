"use strict";

// Copyright 2016 Tamás Demeter-Haludka
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const srcPath = path.join(process.cwd(), "js");

let serverConfig = {};
try {
	serverConfig = require(path.join(process.cwd(), "config.json"));
} catch (e) {
	console.log(e);
}

const baseurl = process.env.BASEURL || serverConfig.baseurl;

const isProd = process.env.NODE_ENV === "production";

const resolve = {
	alias: {},
	root: srcPath,
	extensions: ["", ".js", ".less"],
	modulesDirectories: ["node_modules", "node_modules/abjs/node_modules", "js", "."],
};

const resolveLoader = {
	extensions: ["", ".js"],
	modulesDirectories: ["node_modules", "node_modules/abjs", "node_modules/abjs/node_modules", "js/build"],
};

const moduleLoaders = {
	noParse: [],
	loaders: [
		{
			test: /\.js?$/,
			exclude: [/bootstrap.config.js/, /node_modules\/(?!.*abjs)/],
			loader: "babel-loader",
			query: {
				compact: true,
				presets: ["es2015", "react"],
				plugins: ["transform-export-extensions", "transform-class-properties", "transform-function-bind", "transform-object-rest-spread"],
			},
		},
		{ test: /bootstrap\/js\//, loader: "imports-loader?jQuery=jquery" },
		{ test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,   loader: "url-loader?limit=10000&mimetype=application/font-woff" },
		{ test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,    loader: "url-loader?limit=10000&mimetype=application/octet-stream" },
		{ test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,    loader: "file-loader" },
		{ test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,    loader: "url-loader?limit=10000&mimetype=image/svg+xml" },
		{ test: /\.less$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader") },
		{ test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader") },
		{ test: /.*\.(gif|png|jpe?g|ico)$/i, loader: "file-loader?name=[name]-[sha512:hash:hex:6].[ext]" },
	],
};

const client = {
	target: "web",
	cache: true,
	entry: {
		app: [
			"bootstrap-webpack!"+path.join(process.cwd(), "bootstrap.config.js"),
			path.join(srcPath, "client.js"),
		],
	},
	resolve: resolve,
	output: {
		path: path.join(process.cwd(), "assets"),
		publicPath: baseurl + "assets/",
		filename: "[name].js",
		pathInfo: true,
	},
	module: moduleLoaders,
	plugins: [
		new webpack.DefinePlugin({
			"process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV || "production"),
			"BASEURL": JSON.stringify(baseurl),
		}),
		new ExtractTextPlugin("[name].css"),
		new HtmlWebpackPlugin({
			inject: true,
			template: "html?attrs[]=link:href&-removeOptionalTags!html/index.html",
			chunks: ["app"],
			hash: true,
		}),
		new webpack.NoErrorsPlugin(),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings: false,
				drop_debugger: isProd,
			},
			comments: false,
			mangle: {
				except: [
				],
			},
		}),
	],
	resolveLoader: resolveLoader,
	debug: !isProd,
	devtool: isProd ? null : "source-map",
};

const server = {
	target: "web", // web is better than node, because node-style require is broken
	cache: true,
	entry: {
		app: [
			path.join(srcPath, "server.js"),
		],
	},
	resolve: resolve,
	output: {
		path: path.join(process.cwd(), "worker"),
		filename: "[name].js",
		pathInfo: true,
	},
	module: moduleLoaders,
	resolveLoader: resolveLoader,
	plugins: [
		new webpack.NoErrorsPlugin(),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.optimize.DedupePlugin(),
	],
	debug: !isProd,
};

module.exports = {
	targets: [client, server],
	config: serverConfig,
	srcPath: srcPath,
	isProd: isProd,
	baseurl: baseurl,
};
