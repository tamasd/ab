// Copyright 2016 Tamás Demeter-Haludka
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";

export default class extends React.Component {

	static defaultProps = {
		brand: "",
	};

	render() {
		return (
			<nav className="navbar navbar-inverse">
				<div className="navbar-header">
					<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span className="sr-only">Toggle navigation</span>
						<span className="icon-bar"></span>
						<span className="icon-bar"></span>
						<span className="icon-bar"></span>
					</button>
					<a className="navbar-brand" href="#">{this.props.brand}</a>
				</div>
				<div className="collapse navbar-collapse">
					{this.props.children}
				</div>
			</nav>
		);
	}

	static Left = class extends React.Component {
		render() {
			return (
				<ul className="nav navbar-nav navbar-left">
					{this.props.children}
				</ul>
			);
		}
	}

	static Right = class extends React.Component {
		render() {
			return (
				<ul className="nav navbar-nav navbar-right">
					{this.props.children}
				</ul>
			);
		}
	}

	static Separator = class extends React.Component {
		render() {
			return (
				<li role="separator" className="divider"></li>
			);
		}
	}

	static Item = class extends React.Component {
		render() {
			return (
				<li>{this.props.children}</li>
			);
		}
	}
}
