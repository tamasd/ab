// Copyright 2016 Tamás Demeter-Haludka
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import {Link} from "react-router";
import {t} from "t";

export default class extends React.Component {

	static defaultProps = {
		links: [],
		csrfToken: "",
		className: "",
		childClassName: "",
	};

	render() {
		const links = this.props.links.map((link, i) => {
			const href = link.href.replace("{CSRF_TOKEN}", this.props.csrfToken);

			const item = link.anchor ?
				<a href={href} rel={link.rel}>{t(link.title)}</a> :
				<Link to={href} rel={link.rel}>{t(link.title)}</Link>;

			return <li key={i} className={this.props.childClassName}>{item}</li>;
		});

		return <ul className={"linklist "+this.props.className}>{links}</ul>;
	}

}
