// Copyright 2016 Tamás Demeter-Haludka
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import {capitalizeFirstLetter} from "../util";
import pluralize from "pluralize";

export function createEntityActions(name, extraEndpoints) {
	const upcase = name.toUpperCase();
	const fcup = capitalizeFirstLetter(name);

	let actions = {};

	actions[`load${fcup}`] = function(uuid, skipCache = false) {
		return {
			type: `GET_${upcase}`,
			http: {
				method: "GET",
				url: `/api/${name}/${uuid}`,
				store: pluralize(name),
				cackeKey: uuid,
				skipCache: skipCache,
			},
		}
	};

	actions[`create${fcup}`] = function(entity, skipCache = false) {
		return {
			type: `POST_${upcase}`,
			http: {
				method: "POST",
				url: `/api/${name}`,
				store: pluralize(name),
				data: entity,
				skipCache: skipCache,
			},
		};
	};

	actions[`update${fcup}`] = function(entity, skipCache = false) {
		return {
			type: `PUT_${upcase}`,
			http: {
				method: "PUT",
				url: `/api/${name}/${entity.uuid}`,
				store: pluralize(name),
				data: entity,
				skipCache: skipCache,
			},
		};
	};

	actions[`delete${fcup}`] = function(uuid, skipCache = false) {
		return {
			type: `DELETE_${upcase}`,
			http: {
				method: "DELETE",
				url: `/api/${name}/${uuid}`,
				store: pluralize(name),
				removeKey: uuid,
				skipCache: skipCache,
			},
		};
	};

	Object.keys(extraEndpoints || {}).forEach(function(action) {
		actions[action] = extraEndpoints[action];
	})

	return actions;
};
