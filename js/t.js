// Copyright 2016 Tamás Demeter-Haludka
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import {escapeHTML} from "util";

function isPlural(num) {
	return num > 1;
}

function replace(s, p, param_p) {
	if (p.indexOf("!") !== 0) {
		param_p = escapeHTML(param_p);
	}

	return s.replace(p, param_p);
}

export function t(str, params) {
	return params ? Object.keys(params).reduce((s, p) => {
		return replace(s, p, params[p]);
	}, str) : str;
};

export function pt(str, str_plural, num, params) {
	return isPlural(num) ?
		t(str_plural, params) :
		t(str, params);
};

