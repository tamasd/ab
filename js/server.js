// Copyright 2016 Tamás Demeter-Haludka
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import {renderToString} from "react-dom/server";
import {createMemoryHistory} from "react-router";
import store from "store";
import App from "app";

export default function Server(routes, additionalReducers = null, middlewares = []) {
	$recvSync(function(msg) {
		try {
			const data = JSON.parse(msg);
			const page = renderToString(
				App(
					createMemoryHistory(data.url),
					store(data.state, additionalReducers, middlewares),
					routes
				)
			);
			return JSON.stringify({
				status: 200,
				page: page,
			});
		} catch (ex) {
			return JSON.stringify({
				status: 500,
				page: ex.toString ? ex.toString() : "error",
			});
		}
	});
}
