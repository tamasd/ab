"use strict";

// Copyright 2016 Tamás Demeter-Haludka
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
	styleLoader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader"),
	scripts: {
		"transition": true,
		"alert": true,
		"button": true,
		"carousel": true,
		"collapse": true,
		"dropdown": true,
		"modal": true,
		"tooltip": true,
		"popover": true,
		"scrollspy": true,
		"tab": true,
		"affix": true
	},
	styles: {
		"mixins": true,

		"normalize": true,
		"print": true,

		"scaffolding": true,
		"type": true,
		"code": true,
		"grid": true,
		"tables": true,
		"forms": true,
		"buttons": true,

		"component-animations": true,
		"glyphicons": true,
		"dropdowns": true,
		"button-groups": true,
		"input-groups": true,
		"navs": true,
		"navbar": true,
		"breadcrumbs": true,
		"pagination": true,
		"pager": true,
		"labels": true,
		"badges": true,
		"jumbotron": true,
		"thumbnails": true,
		"alerts": true,
		"progress-bars": true,
		"media": true,
		"list-group": true,
		"panels": true,
		"wells": true,
		"close": true,

		"modals": true,
		"tooltip": true,
		"popovers": true,
		"carousel": true,

		"utilities": true,
		"responsive-utilities": true
	}
};
