"use strict";

// Copyright 2016 Tamás Demeter-Haludka
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const base = require("./webpack.base.js");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const srcPath = path.join(process.cwd());

let client = base.targets[0];
let server = base.targets[1];

client.entry.app.pop();
client.entry.app.push(path.join(srcPath, "client.js"));
client.output.library = "ABClient";
client.output.filename = "client.js";

server.entry.app.pop();
server.entry.app.push(path.join(srcPath, "server.js"));
server.output.library = "ABServer";
server.output.filename = "server.js";

base.targets.forEach(function (target) {
	target.output.path = path.join(process.cwd(), "dist");
	target.output.libraryTarget = "umd";
	target.output.umdNamedDefine = true;
	target.resolve.root = srcPath;

	target.plugins = target.plugins.filter(function (plugin) {
		return !(plugin instanceof HtmlWebpackPlugin);
	});
});

module.exports = base.targets;
