// Copyright 2016 Tamás Demeter-Haludka
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

export function baseUrl() {
	return window.location.protocol + "//" + window.location.hostname + ":" + window.location.port + "/";
}

export function capitalizeFirstLetter(string) {
	return string ? (string.charAt(0).toUpperCase() + string.slice(1)) : "";
}

export const MAXIMUM_ZINDEX = 2147483647;

const entityMap = {
	"&": "&amp;",
	"<": "&lt;",
	">": "&gt;",
	'"': '&quot;',
	"'": '&#39;',
	"/": '&#x2F;'
};

export function escapeHTML(str) {
	return String(str).replace(/[&<>"'\/]/g, function(s) {
		return entityMap[s];
	});
}

