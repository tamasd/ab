// Copyright 2016 Tamás Demeter-Haludka
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

function processHTTPAction(state, action) {
	const storeType = action.http.storeType || "listitem";
	let data = false;
	let id = false;
	if (action.httpSuccess) {
		data = action.http.dataKey ? action.httpSuccess.data[action.http.dataKey] : action.httpSuccess.data;
		id = data.uuid;
	} else if (action.httpError) {
		if (!(action.http.whitelist && action.http.whitelist.indexOf(action.httpError.status) !== -1)) {
			return {
				errors: Object.assign({}, state.errors || {}, {
					[Math.random().toString()]: action.httpError,
				}),
			};
		}
	}
	switch (storeType) {
		case "list":
			return {
				[action.http.store]: data,
			};
			break;
		case "listitem":
			if (id) {
				let list = Object.assign({}, state[action.http.store], {
					[id]: data,
				});
				return {
					[action.http.store]: list,
				};
			} else if (action.http.removeKey) {
				let list = Object.assign({}, state[action.http.store], {
					[action.http.removeKey]: null,
				});
				return {
					[action.http.store]: list,
				};
			}
			break;
		case "singlekey":
			let newState = {
				[action.http.store]: id,
			};
			if (action.http.copyStore && id) {
				newState[action.http.copyStore] = state[action.http.copyStore] || {};
				newState[action.http.copyStore][id] = data;
			}
			return newState;
	}

	return {};
}

export default function(state, action) {
	if (action.http && !action.httpLoading && (action.httpSuccess || action.httpError)) {
		return Object.assign({}, state, processHTTPAction(state, action));
	}
	return state;
};
