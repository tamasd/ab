// Copyright 2016 Tamás Demeter-Haludka
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

export function request(method, url, body, headers = null) {
	if (!headers) {
		headers = new Headers();
	}
	if (!headers.has("Content-Type")) {
		headers.set("Content-Type", "application/json");
	}
	if (!headers.has("Accept")) {
		headers.set("Accept", "application/hal+json; q=0.9, application/json; q=0.8");
	}

	return fetch(url, {
		method: method,
		headers: headers,
		body: body,
		credentials: "include",
		cache: "no-cache",
		redirect: "follow",
	}).then(function (response) {
		if (!response.ok) {
			return Promise.reject(response);
		}

		return Promise.all([response, response.text()]);
	}).then(function (values) {
		let [response, body] = values;
		if (body && body.indexOf(")]}',\n") === 0) {
			body = body.substr(6);
		}
		return {
			response: response,
			data: body ? JSON.parse(body) : null,
		};
	});
};

export function get(url, headers = null) {
	return request("GET", url, null, headers);
};

export function post(url, body, headers = null) {
	return request("POST", url, body, headers);
};

export function put(url, body, headers = null) {
	return request("PUT", url, body, headers);
};

export function remove(url, headers = null) {
	return request("DELETE", url, null, headers);
};
