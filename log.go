// Copyright 2015 Tamás Demeter-Haludka
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ab

import (
	"bytes"
	"io"
	"net/http"
	"os"

	"github.com/agtorre/gocolorize"
	"gitlab.com/tamasd/ab/lib/log"
)

const logKey = "ablog"
const logBufKey = "ablogbuf"

// DefaultLoggerMiddleware creates created a LoggerMiddleware with the recommended settings.
func DefaultLoggerMiddleware(level log.LogLevel) func(http.Handler) http.Handler {
	return LoggerMiddleware(
		level,
		log.UserLogFactory,
		log.VerboseLogFactory,
		log.TraceLogFactory,
		os.Stdout,
	)
}

// LoggerMiddleware injects a logger and a per request log buffer into the request context.
func LoggerMiddleware(level log.LogLevel, userLogFactory, verboseLogFactory, traceLogFactory func(w io.Writer) log.Logger, lw io.Writer) func(http.Handler) http.Handler {
	colorizer := gocolorize.NewColor("red")
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			buf := bytes.NewBuffer(nil)
			mw := io.MultiWriter(buf, lw)
			l := log.NewLogger(
				userLogFactory(mw),
				verboseLogFactory(mw),
				traceLogFactory(mw),
			)
			l.Level = level
			if reqid := GetRequestID(r); reqid != "" {
				l.AddPrefix(colorizer.Paint(reqid) + " ")
			}

			r = SetContext(r, logKey, l)
			r = SetContext(r, logBufKey, buf)

			next.ServeHTTP(w, r)
		})
	}
}

// RequestLogs returns the contents of the log buffer of the request.
func RequestLogs(r *http.Request) string {
	return r.Context().Value(logBufKey).(*bytes.Buffer).String()
}

func logFromContext(r *http.Request) *log.Log {
	return r.Context().Value(logKey).(*log.Log)
}

// LogUser returns the user level logger from the request context.
func LogUser(r *http.Request) log.Logger {
	return logFromContext(r).User()
}

// LogVerbose returns the verbose level logger from the request context.
func LogVerbose(r *http.Request) log.Logger {
	return logFromContext(r).Verbose()
}

// LogTrace returns the trace level logger from the request context.
func LogTrace(r *http.Request) log.Logger {
	return logFromContext(r).Trace()
}
