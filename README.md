# Alien Bunny

Web development platform.

## Quick start

To install:

	go get -u gitlab.com/tamasd/ab/cmd/abt

To install the frontend:

	npm install --save abjs

Alternatively, if you want to work on the development version, use [npm link](https://docs.npmjs.com/cli/link):

	cd $GOPATH/gitlab.com/tamasd/ab/js
	npm link
	cd $YOUR_PROJECT
	npm link abjs

To have full universal JS support, see the [V8 subproject](https://gitlab.com/tamasd/abv8).

See examples and more information at [the project website](http://www.alien-bunny.org).

## Requirements

* Go 1.7.
* Built-in functionalities using PostgreSQL require PostgreSQL 9.5 or newer.
* Frontend components and the scaffolded application base require NPM 3+.

## The abt command

The `abt` command is a helper tool for the development. Subcommands:

* `watch`: starts a proxy that builds and reruns your application when you save a file.
* `gensecret`: generates a secret key. Useful for generating cookie secrets.
* `decrypt`: decrypts a string with the util package

## Contributing

Feel free to open an issue / pull request for discussion, feature request or bug report.

[![build status](https://gitlab.com/tamasd/ab/badges/master/build.svg)](https://gitlab.com/tamasd/ab/commits/master) [![coverage report](https://gitlab.com/tamasd/ab/badges/master/coverage.svg)](https://gitlab.com/tamasd/ab/commits/master)
